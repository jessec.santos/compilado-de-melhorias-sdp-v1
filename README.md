# Compilado de Melhorias SDP - v1

# Introdução

Esta documentação foi elaborada de forma que contribuísse para a melhoria da plataforma SDP em seus próximos releases. 

Os insights aqui reunidos foram de clientes e usuários recorrentes da plataforma, alguns também foram ideias que possam agregar valor e melhorar à experiência daquele que estiver navegando pelo SDP.

Foi-se decidido por fazer uma divisão nesta DOC, onde abordaremos duas questões principais: <strong>User Experience</strong> e em seguida <strong>Arquitetura de Dados</strong>.

# User Experience

## Visão Geral

- ### <strong>Sessão Finalizando Repentinamente</strong>
	<em>Algumas vezes ao se estar utilizando o SDP a sessão é finalizada, retornando à tela de Login repentinamente.</em>

- ### <strong>Garantir que os Pods não Caiam</strong>
	<em>Este item se aplica à todas as ferramentas da plataforma, porém foi relatado acontecendo mais no Superset, Jupyter e no caso da Travelex, no NiFi. Além da queda do ambiente, também foram relatadas situações de lentidões extremas. Este problema atrapalha muito a experiência do usuário na plataforma e seu progresso no que estiver fazendo dentro dela.</em>
	
- ### <strong>Facilitar o processo de Criação de Ambiente/Users</strong>
	<em>Atualmente para criar um novo ambiente é necessário enviar uma mensagem para API de Registro que então notificará o usuário em seu email com o link da criação de ambiente, porém, para tornar o produto escalável, seria melhor ter uma forma que o usuário pudesse criar seu ambiente de forma autônoma, sem a necessidade de entrar em contato com a Semantix.</em>

- ### <strong>Tutorial Amigável das Ferramentas</strong>
	<em>Apesar da plataforma possuir uma documentação, seria interessante possuir em cada tela/item um símbolo de interrogação "?" onde o usuário, ao passar o mouse em cima, surgiria um pop-up explicando de forma simples e amigável qual a funcionalidade daquela ferramenta e como usá-la. Além disso, adicionar uma funcionalidade de "Tour" pela plataforma ajudaria muito os usuários novos a se habituarem a respeito do SDP. Esta feature ajudaria o usuário a ter muito mais autononima e confiança ao navegar pelo SDP.</em>

- ### <strong>Opção de Reduzir o Tamanho do Menu Lateral</strong>
	<em>Para uma melhor experiência do usuário, seria interessante dar a opção de reduzir o tamanho do menu lateral. Alguns usuários relataram que ao se utilizar o Superset e o JupyterLab sentiram a tela muito pequena, o que apesar de não prejudicar seu desempenho, tornou sua experiência com a plataforma desagradável e menos imersiva.</em>

- ### <strong>Alterar Senha</strong>
	<em>Por enquanto não é possível alterar a senha do usuário através da plataforma, esta é uma feature essencial a ser adicionada o mais rápido possivel, garantindo assim, a segurança das informações contidas no SDP.</em>

- ### <strong>Vizualização do Datalake Simplificada</strong>
	<em>Atualmente para se verificar o que há dentro do DataLake é necessário executar queries dentro do Superset ou JupyerLab. Seria interessante conseguir vizualizar de forma simples todo o conteúdo do Datalake em tempo real através da interface sem precisar executar nenhuma querie.</em>

- ### <strong>Divisão de Roles dentro da Plataforma</strong>
	<em>Para melhor controle e segurança dos dados alocados na plataforma, seria interesse dar ao administrador do ambiente a capacidade de gerir as roles de cada usuário. Gerindo por permissões de acessos às ferramentas e possíveis manipulações dentro das mesmas. Este item foi sugerido por mais de um cliente.</em>

- ### <strong>Criação camada RAW e TRUSTED não funcionando</strong>
	<em>Foi-se relatado que em alguns casos, a criação das camadas RAW e TRUSTED não ocorreram de forma automática, delegando esta função ao próprio usuário.</em>

## Tela Pipeline - Ingestão

- ### <strong>Vizualização de Logs</strong>
	<em>Este item talvez tenha sido o mais apontado por toda a pesquisa a respeito de melhorias na plataforma. Atualmente não é possível saber com certeza se as ingestões criadas estão sendo executadas ou se houve algum erro. A unica forma de validar isto é checando através de queries pelo Superset e JupyterLab, o que torna a experiência do usuário muito desagradável. Além disso, ao falhar a conexão com algum data source externo, não há nenhum log que retorne que o erro ocorreu, simplesmente nenhum resource é mostrado na tela. Seria interessante ter uma aba de logs dentro de cada pipeline e no menu onde se encontra todos os pipelines, mostrar quando foi a última vez que uma ingestão ocorreu e se foi bem sucedida ou se falhou.</em>

- ### <strong>Pesquisa dentro de Resources</strong>
	<em>Ao se conectar em um data source, podemos escolher qual tabela dentro do mesmo queremos ingerir para o Datalke. Entretanto, se por acaso obtivermos muitas tabelas disponíveis, não é possível filtrar pelo nome da mesma, sendo necessário procurar uma por uma até encontrar àquela desejada para a ingestão.</em>

- ### <strong>Novo Pipeline com Credenciais Existentes</strong>
	<em>Seria interessante dar ao usuário a opção de utilizar credenciais de conexão que já estão sendo executadas dentro da Tela de Pipeline. Atualmente, ao se criar um novo Pipeline é necessário digitar todas as credencias de conexão novamente, sendo um trabalho desnecessário por parte do usuário.</em>

- ### <strong>Pipeline Continua Mesmo Após ser Desabilitado</strong>
	<em>Foi relatado que mesmo após desabilitar um pipeline, o mesmo continuou executando no dia seguinte.</em>

- ### <strong>Pré-Query</strong>
	<em>Foi-se sugerida a implementação de um sistema de pré-query durante o processo de pipeline, tornando possível diminuir o tamanho das ingestões recorrentes e a realização de tratamenos prévios.</em>

## Tela Transform - Transformação

- ### <strong>Logs</strong>
	<em>Ao se criar um fluxo de transformação, não é possível verificar através da tela que a mesma ocorreu corretamente, seria interessante para o usuário algum indicativo que a tranformação está ocorrendo e que foi bem sucedida.</em>

- ### <strong>Tela Pouco Intuitiva</strong>
	<em>O fluxo de edição de um Transformation está muito pouco intuitivo. Ao passar um novo item da camada raw para a trusted, é necessário obrigatoriamente editar o nome e o número de partições nas configurações do item. Porém, essa aba de configurações só aparece se clicar no item que foi para a coluna trusted. Apenas passar o item para a coluna trusted não abre as configs do item. Além disso, é possível deixar os campos em branco, porém o botão "save" no canto superior direito não fica ativo.</em>


## Tela Sandbox - Manipulação

- ### <strong>Scripts Padrões</strong>
	<em>Para o conforto do usuário, seria interessante o JupyterLab vir com scripts de tratamentos e higienizações padrões. Neste caso, seria um compilado de funções pré-definidas, ex: Validação de datas, Validação de CPFs, remoção de palavrões etc</em>

- ### <strong>Instalação de Dependências</strong>
	<em>Talvez isso não tenha solução pois é a forma que o Kernel do Jupyterlab funciona, porém, caso tenha, seria interessante não precisar instalar todas as dependências toda vez que o Sandbox é iniciado, uma vez seria o suficiente.</em>

- ### <strong>Salvar Progresso Automaticamente</strong>
	<em>Algumas vezes depois de utilizar o Sandbox, o progresso não era salvo automaticamente. Apesar de possuir a opção de salvar, seria interessante todo o progresso ser salvo de forma que o usuário não tenha que se preocupar enquanto navega na plataforma.</em>

- ### <strong>Execução Schedulada</strong>
	<em>Seria interessante ter a opção de definir schedules para a execução do notebook. Talvez isso seja implementado em Jobs, mas de qualquer forma, é algo que torna o Sandbox mais versátil.</em>

# Arquitetura de Dados













